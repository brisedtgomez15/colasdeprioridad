/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

public class TrabajoDeImpresion implements Comparable<TrabajoDeImpresion> {

    private int cHoja;
    private long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public TrabajoDeImpresion(int cHoja, long time) {
        this.cHoja = cHoja;
        this.time = time;
    }

    public int getcHoja() {
        return cHoja;
    }

    public void setcHoja(int cHoja) {
        this.cHoja = cHoja;
    }

    @Override
    public int compareTo(TrabajoDeImpresion o) {
        if (o.getcHoja() < this.getcHoja()) {
            return 1;
        } else if (o.getcHoja() > this.getcHoja()) {
            return -1;
        } else {
            if (time > o.getTime()) {
                return 1;
            } else if (time < o.getTime()) {
                return -1;
            }
            return 0;
        }

    }

}
